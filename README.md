# DogPound exercise

## Getting started
- Fork this project, using the [Fork] button in gitlab
- Clone the project locally (If you use IDEA, it should complile once you change the Java version to 16)
- Resolve the exercise (suggestion: do a commit after each exercise step)
- Update JavaDoc and add an UML diagram for the whole project
- Do the final commit and push it to Gitlab

When you are done:
- Paste your project URL in Moodle
- [OPT] Create a merge request

## Notes
This is an INDIVIDUAL assignment. Do not copy from colleagues. Transgressors will be persecuted, tortured, and hanged. 

## Exercise part 1 - Inheritance

### Subclass behaviour
Add statements in DogTest.java after you create and print the dog to create and print a Yorkshire and a Labrador. Note
that the Labrador constructor takes two parameters: the name and color of the labrador, both strings. Don't change any
files besides DogTest.java. Now recompile DogTest.java; you should get an error saying something like
```
./Labrador.java:18: Dog(java.lang.String) in Dog cannot be applied to ()
{
^
1 error
```
If you look at line 18 of Labrador.java it's just a {, and the constructor the compiler can't find (Dog()) isn't called
anywhere in this file.
- What's going on? State the nature of the problem as a comment in the code (Hint: What call must be made in the constructor of a subclass?)
- Fix the problem (which really is in Labrador) so that DogTest.java creates and makes the Dog, Labrador, and
Yorkshire all speak. This can be done in several ways. 

### Subclass methods

Add code to DogTest.java to print the average breed weight for both your Labrador and your Yorkshire. Use the
`avgBreedWeight()` method for both. 
- What error do you get? Why? Again, state it as a comment and fix it.
- Fix the problem by adding the needed code to the and Labrador and Yorkshire class.

### Static methods
Add a static int avgBreedWeight() method to the Dog class. Add the lines in the main() method that print the average breed weight of all Dog classes. Since Dog has no idea what breed it is, you should figure out what the output of this method will be.


## Exercise part 2 - Collections

### Cage
Create the Cage class, which is able to hold  any type of dog. 

You should add methods that behave like this:

- `add(...)`: adds a Dog to Cage
- `remove(...)` removes a Dog, searching it by NAME
- `toString()` : returns the content of the Cage in a good-looking way (suprise us!) 
- `chaos()`: outputs the barking of all dogs in the cage

In the `main() `method, create three more dogs, and add them all to a Cage.
### Specialized Cage
Create the LabradorCage class, which is able to hold Only labradors. If you are lazu enough, 
you should only write a couple of lines.

In the `main()` method, crate one more Labrador, add it to the specialized cage and print the content.

### Transfer

In the LabradorCage class, add the `transfer(...)` method (you can choose if it is static on not).

This method takes all Labradors from a Cage and transfers them to a LabradorCage. 

In the `main()` method add lines of code that test this new method


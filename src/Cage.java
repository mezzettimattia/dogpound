import java.util.ArrayList;

public class Cage {
    private ArrayList<Dog> dogs = new ArrayList<Dog>();

    public void add(Dog dog){
        dogs.add(dog);
    }
    public void remove(String name){
        for(int i=0;i<dogs.size();i++){
            if(dogs.get(i).getName().equals(name)){
                dogs.remove(i);
            }
        }
    }

    @Override
    public String toString() {
        return "Cage{" +
                "dogs=" + dogs +
                '}';
    }
    public void chaos(){
        for (Dog i:dogs ) {
            System.out.println(i.speak());
        }
    }

}

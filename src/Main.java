public class Main {

    public static void main(String[] args)
    {
        Dog dog = new Dog("Spike");
        System.out.println(dog.getName() + " says " + dog.speak());

        Yorkshire yorkshire=new Yorkshire("Antonio");
        System.out.println(yorkshire.getName() + " says " + yorkshire.speak());

        Labrador labrador=new Labrador("Aron"," brown ");
        System.out.println(labrador.getName() + " says " + labrador.speak());

        System.out.println("Average dog weight: " + Dog.avgBreedWeight() );
        System.out.println("Average Yorkshire weight: " + Yorkshire.avgBreedWeight() );
        System.out.println("Average Labrador weight: " + Labrador.avgBreedWeight() );

        Labrador labrador1=new Labrador("Cane1","White");
        Labrador labrador2=new Labrador("Cane2","Black");
        Labrador labrador3=new Labrador("Cane3","Yellow");



    }
}
